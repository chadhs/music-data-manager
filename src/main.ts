#!/usr/bin/env node

import { Command } from "commander";
import fs from "fs";
import path from "path";

const program = new Command();

program
  .version("0.0.1")
  .description("music data manager")
  .requiredOption("-i, --input-file <value>", "Input file")
  .requiredOption("-c, --changes-file <value>", "Changes file")
  .option("-o, --output-file <value>", "Output file")
  .parse(process.argv);

const options = program.opts();

const inputData = options.inputFile
  ? JSON.parse(fs.readFileSync(options.inputFile).toString())
  : {};
const changesData = options.changesFile
  ? JSON.parse(fs.readFileSync(options.changesFile).toString())
  : {};
const outputFile = options.outputFile ? options.outputFile : "output.json";

function getMaxId(entries) {
  // to create a new user, song, or playlist; we want to know how to increment the id
  const eIds = entries.map((entry) => parseInt(entry.id));
  return Math.max(...eIds);
}

function addSongsToPlaylists(playlists) {
  // takes an array of playlists and returns an array of playlists with updates applied
  const changes = changesData.playlists.update.add;
  const updates = changes.map((change) => {
    const playlist = playlists.find((p) => p.id === change.playlist_id);
    const updatedPlaylist = {
      id: playlist.id,
      owner_id: playlist.owner_id,
      songs_ids: [...playlist.song_ids, ...change.song_ids],
    };
    return updatedPlaylist;
  });
  const updatedIds = updates.map((update) => update.id);
  const filteredPlaylists = playlists.filter(
    (playlist) => !updatedIds.includes(playlist.id),
  );
  return [...filteredPlaylists, ...updates];
}

function addNewPlaylists(playlists) {
  // takes an array of playlists and returns an array of playlists with new lists added
  const changes = changesData.playlists.create;
  let newId = getMaxId(playlists);
  const updates = changes.map((change) => {
    newId++;
    return {
      id: newId.toString(),
      owner_id: change.owner_id,
      song_ids: [...change.song_ids],
    };
  });
  return [...playlists, ...updates];
}

function deletePlaylists(playlists) {
  // takes an array of playlists and returns an array of playlists with deletions removed
  const changes = changesData.playlists.delete.playlist_ids;
  const updates = playlists.filter((p) => !changes.includes(p.id));
  return updates;
}

const playlists = inputData.playlists;
const playlistsWithUpdates = addSongsToPlaylists(playlists);
const playlistsWithAdditions = addNewPlaylists(playlistsWithUpdates);
const playlistsFiltered = deletePlaylists(playlistsWithAdditions);

const output = {
  users: inputData.users,
  songs: inputData.songs,
  playlists: playlistsFiltered,
};

function writeOutputFile(output) {
  fs.writeFile(outputFile, JSON.stringify(output), (error) => {
    if (error) {
      console.log(`error writing to output file ${outputFile}`);
      return;
    }
    console.log(`output file ${outputFile} written successfully`);
  });
}

writeOutputFile(output);
