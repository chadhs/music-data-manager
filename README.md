# music data manager


## summary
This is a command line utility to apply changes to Spotify JSON data locally.  It takes in a spotify.json file of existing data and a changes.json file to apply to it, resulting in an output.json data file, leaving the original intact.

### choices and assumptions
I chose to design the changes.json data file and the command line utility in a way that allowed for one or many changes of each supported type.  This first iteration allows for three operation types:

- updating a playlist
  - adding songs to a playlist
  - explicitly setting the songs for a playlist (replacing the entire list)
- creating a new playlist
- deleting a playlist

### time spent building
I gave myself a two hour development time box for this project.  I did end up going an hour or so over, mostly tinkering with trouble-shooting some build problems as I do not commonly build command-line interfaces.

### improvements
- The write file operation does have error-checking, but is synchronous, blocking the event loop; this could be improved.
- The read file operations could use error-checking and be refactored to be asynchronous calls as well.
- Add support for user and song management.
- Break up the code into separate namespaces: common, user, song, playlist.
- Add unit tests for different common use cases to validate results; currently diffing or "stare and compare" are the only validation methods.

### how i could scale this
- Sub commands for users, songs, and playlists could be added allowing multiple instance of the application to be run.
- The program could be split into a service and a client.  The service could be daemonized using the pm2 module allowing it to always run and leverage multiple CPUs.  The service could be refactored to process updates placed on a queue.  The client would operate much like the current iteration, but would place update batches as jobs on a the services queue to be processed.  This would take some additional hammock time and prototyping experimentation.


## development

This repo contains both a sample spotify.json and changes.json file as a base to work with.
You will need to have node.js and npm installed to develop, build, and run this application.

### making changes

Make any changes you wish and then run the app using:

```sh
npx ts-node . -i "spotify.json" -c "changes.json" -o "output.json"
```

### testing

Using Jest you can write tests, update your code, and see the output live using the following command:

```sh
npm install && npm run test
```


## build and run

Make sure you are in the project root directory before proceeding.

### build the cli app

```sh
npm install && npm run build && npm install -g .
```

### run the cli app

```sh
musicdm -i spotify.json -c changes.json -o output.json
```

### errors

For subsequent builds it may be necessary to clean your build environment and reinstall.
```sh
npm run clean && npm install && npm run build && npm uninstall -g music-data-manager && npm install -g .
```
